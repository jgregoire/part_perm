//
// Program:		Part Permutator
// Description:	Take all options for a part number and output a list of all possible
//				combinations of options.
// Author:		James Gregoire (JFG)
// Written:		Sept. 2020
//

// TODO:
//	* File output.
//  * Size limit.
//  * Support a blank option "".

use std::io;

// Recursive function to find all the permutations of concatanated member strings.
// Example: [["a", "b"], ["c", "d"], ["e", "f"]] -> ["ace", "acf", "ade", "adf", "bce", "bcf", "bde", "bdf"]
// Careful, large input yields enormous output.
fn permute(input_vec: Vec<Vec<String>>) -> Vec<String>
{
	// Do we need to go deeper?
	if input_vec.len() > 1
	{
		// Pop the last n-1 elements into a new vector.
		let tail: Vec<Vec<String>> = input_vec
										.to_vec()
										.drain(1..)
										.collect();
										
		let head: Vec<String>      = input_vec[0].to_vec();

		// Now we start recursively concatenating and storing results in return_vec.
		let mut return_vec: Vec<String> = Vec::new();

		// Turn tail into a permuted Vec<String>, and permute that with head..
		for head_str in head
		{
			for tail_str in permute(tail.to_vec())
			{
				// We need to make a copy that we can modify.
				let mut new_head_str = head_str.to_string();

				// Concatenate with a string from tail.
				new_head_str.push_str(tail_str.as_str());

				// Append that to our return vector.
				return_vec.push(new_head_str.to_string());
			}
		}

		// Return a larger Vec<String>.
		return_vec.to_vec()
	}
	else
	{
		// We have reached the bottom. Return the remaining Vec<String> in input_vec.
		input_vec[0].to_vec()
	}
}

fn main()
{
    println!("How many options does your part have?");
    println!("Ex: T513X336K035CH62107280 breaks into");
    println!("    T-513-X-336-K-035-C-H-62-10-7280, 11 options.");

    let mut num_opts_in = String::new();

	// Get number of options.
    println!("\nNumber: ");

	// Read a line of input (String).
    io::stdin()
    	.read_line(&mut num_opts_in)
    	.expect("Failed to read line!"); // Actual error handling would take effort. Pssh.

	// Convert it to a number.
	let num_opts: i32 = num_opts_in
		.trim()
		.parse()
		.expect("Please enter a number!"); // It's rude to crash like this, but hey, the user was rude first.

	// Now get the set of choices for each option.
	// Rust Vectors are like Lists in Python or Lua, but a little more rigorous.
	let mut options: Vec<Vec<String>> = Vec::new();

    // Get options. Humans like to start at 1, not 0.
    for i in 1..num_opts+1
    {
    	println!("\nEnter choices for Option {}, space-delimited:", i);

		//	Get a string of choices, hopefully space-delimited.
		// No hand-holding here. Garbage in, garbage out.

		// This will hold a line of input.
    	let mut choices = String::new();

		// Read a line of input.
    	io::stdin()
    		.read_line(&mut choices)
    		.expect("Failed to read line!");

		// Split that String into a Vec<String>
		let mut option: Vec<String> = Vec::new();

		// Add each option to a vector as its own string.
    	for choice in choices
    				.trim()
    				.split_whitespace() // Lookit this handy little function!
    	{
    		option.push(choice.to_string());
    	}

		// Now append that option to our set of options.
    	options.push(option);
    }

	// *accusing tone* Now look at what you've done!
    println!("\nOptions: {:?}\n", options);

    // Now generate all the permutations.
    let result: Vec<String> = permute(options);

	println!("Permutations:");

    // Print the result nicely.
	for part_num in result
	{
		println!("{}", part_num.as_str());
	}

	// That's all, folks!
}
